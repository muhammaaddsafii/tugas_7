<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = ['lecturer_id', 'slug', 'strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai'];

    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }
}
