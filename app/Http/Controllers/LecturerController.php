<?php

namespace App\Http\Controllers;

use App\History;
use App\Lecturer;
use Illuminate\Http\Request;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lecturers = Lecturer::latest()->paginate(5);
        return view('lecturer.index', ['lecturers' => $lecturers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lecturer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'nip' => 'required',
            'gelar' => 'required',
        ]);

        $attr['slug'] = \Str::slug($request->nama);

        $lecturer = Lecturer::create($attr);

        $histo = $request->validate([
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required',
        ]);

        $histo['lecturer_id'] = $lecturer->id;

        $histo['slug'] = \Str::slug($request->strata);

        History::create($histo);

        return redirect()->to('dosen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturers)
    {
        return view('lecturer.edit', ['lecturers' => $lecturers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lecturer $lecturers, History $histories)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'nip' => 'required',
            'gelar' => 'required',
        ]);

        $attr['slug'] = \Str::slug($request->nama);

        $lecturers->update($attr);

        $histo = $request->validate([
            'strata' => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai' => 'required',
            'tahun_selesai' => 'required',
        ]);

        $histo['lecturer_id'] = $lecturers->id;

        $histo['slug'] = \Str::slug($request->strata);

        $histories->update($histo);

        return redirect()->to('dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
