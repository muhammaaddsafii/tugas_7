<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create()
    {
        $subjects = Subject::get();
        return view('register.index', ['subjects' => $subjects]);
    }
}
