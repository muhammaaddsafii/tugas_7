<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $fillable = ['nama', 'slug', 'nip', 'gelar'];

    public function histories()
    {
        return $this->hasMany(History::class);
    }
}
