<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['nama', 'slug', 'nim', 'jenis_kelamin', 'ttl'];

    public function classes()
    {
        return $this->hasMany(Kelas::class);
    }
}
