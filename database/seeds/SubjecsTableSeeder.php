<?php

use Illuminate\Database\Seeder;

class SubjecsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = collect([
            'sistem informasi',
            'pemrograman dasar',
            'pemrograman web',
            'basis data',
            'pengantar teknologi',
            'sistem operasi',
            'rekayasa perangkat lunak',
            'jaringan komputer',
            'algoritma dan struktur data',
            'user experience',
            'etika dan bisnis informasi'
        ]);
        $subjects->each(function ($matkul) {
            \App\Subject::create([
                'nama' => $matkul,
                'slug' => \Str::slug($matkul),
                'jumlah_sks' => rand(2, 4),
            ]);
        });
    }
}
