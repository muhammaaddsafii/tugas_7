@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Welcome !!</h4>
            </div>
            <div class="card-body d-flex align-items-center">
                <h5>
                    Selamat Datang di Sistem Informasi Kampus
                </h5>
            </div>
        </div>
    </div>
@endsection
