<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Kampus</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendors/iconly/bold.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rainbow.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/bootstrap-icons/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.svg" type="image/x-icon')}}">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>

<body>
    <div id="app">
        <div id="sidebar" class='active'>
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a>Sistem Informasi Kampus</a>
                        </div>
                        <div class="toggler">
                            <a href="#" class='sidebar-hide d-xl-none d-block'><i class='bi bi-x bi-middle'></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class='sidebar-title'>Menu</li>
                        <li class="sidebar-item {{ request()->is('dosen') ? ' active' : ''}}">
                            <a href="/dosen" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dosen</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('mahasiswa') ? ' active' : ''}}">
                            <a href="/mahasiswa" class='sidebar-link'>
                                <i class="bi bi-palette-fill"></i>
                                <span>Mahasiswa</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('mata-kuliah') ? ' active' : ''}}">
                            <a href="/mata-kuliah" class='sidebar-link'>
                                <i class="bi bi-filter-right"></i>
                                <span>Mata Kuliah</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('pendaftaran-mata-kuliah') ? ' active' : ''}}">
                            <a href="/pendaftaran-mata-kuliah" class='sidebar-link'>
                                <i class="bi bi-cash"></i>
                                <span>Pendaftaran Mata Kuliah</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>

        @yield('content')

        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="float-start">
                    <p>2021 &copy; Sistem Informasi Kampus</p>
                </div>
                <div class="float-end">
                    <p>Created <span class='text-danger'></span> by : The Rotten Bug</a></p>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <script src="{{asset('assets/js/extensions/rainbow-custom.min.js')}}"></script>
    <script src="{{asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('assets/vendors/apexcharts/apexcharts.js')}}"></script>
    <script src="{{asset('assets/js/pages/dashboard.js')}}"></script>

    <script src="{{asset('assets/js/main.js')}}"></script>

    <!-- untuk selec2 -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- untuk selec2 -->

    <script>
    $(document).ready(function() {
        $('.select2').select2();
        })
    </script>
</body>

</html>
