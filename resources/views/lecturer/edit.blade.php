@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Dosen</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Edit Data Dosen</h4>
            </div>
            <div class="card-body">
                <form action="/dosen/{{ $lecturers->slug }}/edit" method="POST">
                    @method('patch')
                    @csrf
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama') ?? $lecturers->nama }}">
                    </div>
                    <div class="mb-3">
                        <label for="nip" class="form-label">NIP</label>
                        <input type="text" name="nip" class="form-control" id="nip" value="{{ old('nip') ?? $lecturers->nip }}">
                    </div>
                    <div class="mb-3">
                        <label for="gelar" class="form-label">Gelar</label>
                        <input type="text" name="gelar" class="form-control" id="gelar" value="{{ old('gelar') ?? $lecturers->gelar }}">
                    </div>
                    <h6 class="text-secondary mb-3">Riwayat Pendidikan</h6>
                    @foreach ($lecturers->histories as $history)
                    <div class="mb-3">
                        <label for="strata" class="form-label">Strata</label>
                        <input type="text" name="strata" class="form-control" id="" value="{{ old('strata') ?? $history->strata }}">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="jurusan" class="form-label">Jurusan</label>
                                <input type="text" name="jurusan" class="form-control" id="" value="{{ old('jurusan') ?? $history->jurusan }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="sekolah" class="form-label">Sekolah</label>
                                <input type="text" name="sekolah" class="form-control" id="" value="{{ old('sekolah') ?? $history->sekolah }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="tahun_mulai" class="form-label">Tahun Mulai</label>
                                <input type="text" name="tahun_mulai" class="form-control" id="" value="{{ old('tahun_mulai') ?? $history->tahun_mulai }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="tahun_selesai" class="form-label">Tahun Selesai</label>
                                <input type="text" name="tahun_selesai" class="form-control" id="" value="{{ old('tahun_selesai') ?? $history->tahun_selesai }}">
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="d-flex flex-row-reverse">
                        <button type="button" class="btn btn-sm btn-success rounded-pill" id="addMore">Add More</button>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm rounded-pill">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
