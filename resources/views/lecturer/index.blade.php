@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Dosen</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Daftar Dosen</h4>
                <a href="/dosen/create-new" class="btn btn-sm btn-primary rounded-pill" href="">Tambah Dosen</a>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th class="text-center">Nama</th>
                            <th class="text-center">NIP</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lecturers as $lecturer)
                        <tr>
                            <td class="text-wrap text-center">{{ $lecturer->nama }}</td>
                            <td class="text-wrap text-center">{{ $lecturer->nip }}</td>
                            <td class="d-flex justify-content-center">
                                <a class="btn btn-warning btn-sm rounded-pill mx-2" href="/dosen/{{ $lecturer->slug }}/edit">Edit</a>
                                <a class="btn btn-danger btn-sm rounded-pill" href="">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
