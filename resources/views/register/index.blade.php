@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Pendaftaran Mata Kuliah</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header">
                <h4 class='card-title'>Form Pendaftaran Mata Kuliah</h4>
                <h6 class="text-secondary">Isi form berikut untuk mendaftar mata kuliah yang diinginkan</h6>
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    @csrf
                    <div class="mb-4">
                        <label for="nama" class="form-label">Nama Mahasiswa</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="mb-4">
                        <label for="subjects">Mata Kuliah yang ingin diambil</label>
                        <select name="subjects[]" id="subjects" class="form-control select2" multiple>
                            @foreach ($subjects as $subject)
                                <option value="{{ $subject->id }}">{{ $subject->nama }} [{{ $subject->jumlah_sks}} sks] </option>
                            @endforeach
                        </select>
                    </div>
                    <p class="d-flex flex-row-reverse text-secondary">note : Jumlah SKS maksimal adalah 24</p>
                    <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
