@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Mahasiswa</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Tambah Data Mahasiswa</h4>
            </div>
            <div class="card-body">
                <form action="{{url('/mahasiswa/store')}}" method="POST">
                    @csrf
                    <div class="mb-4">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                        @error('nama')
		                <div class="mb-2 text-danger">
			                {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="nim" class="form-label">NIM</label>
                        <input type="text" name="nim" class="form-control" id="nim">
                        @error('nim')
		                <div class="mb-2 text-danger">
			                {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                        <select class="form-select" name="jenis_kelamin" id="">
                            <option value="" disabled selected>Pilih Jenis Kelamin</option>
                            <option value="laki-laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
		                <div class="mb-2 text-danger">
			                {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <label for="ttl" class="form-label">Tempat, Tanggal Lahir</label>
                        <input type="text" name="ttl" class="form-control" id="ttl" placeholder="Contoh : Jakarta, 12 Mei 1999">
                        @error('ttl')
		                <div class="mb-2 text-danger">
			                {{$message}}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
