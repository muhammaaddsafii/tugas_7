@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Mahasiswa</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Daftar Mahasiswa</h4>
                <a href="/mahasiswa/create-new" class="btn btn-sm btn-primary rounded-pill" href="">Tambah Mahasiswa</a>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th class="text-center">Nama</th>
                            <th class="text-center">NIM</th>
                            <th class="text-center">Jenis Kelamin</th>
                            <th class="text-center">Tempat, Tanggal Lahir</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($students as $student)
                        <tr>
                            <td class="text-wrap text-center">{{ $student->nama }}</td>
                            <td class="text-wrap text-center">{{ $student->nim }}</td>
                            <td class="text-wrap text-center">{{ $student->jenis_kelamin }}</td>
                            <td class="text-wrap text-center">{{ $student->ttl }}</td>
                            <td class="d-flex justify-content-center">
                                <a class="btn btn-warning btn-sm rounded-pill mx-2" href="/mahasiswa/{{$student->slug}}/edit">Edit</a>
                                <a class="btn btn-danger btn-sm rounded-pill" href="">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
