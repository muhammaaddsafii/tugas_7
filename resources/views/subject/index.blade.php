@extends('layout.master')

@section('content')
<div id="main">
    <header class='mb-3'>
        <a href="#" class='burger-btn d-block d-xl-none'>
            <i class='bi bi-justify fs-3'></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Mata Kuliah</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h4 class='card-title'>Daftar Mata Kuliah</h4>
                <a href="" class="btn btn-sm btn-primary rounded-pill" href="">Tambah Mata Kuliah</a>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Jumlah SKS</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subjects as $subject)
                        <tr>
                            <td class="text-wrap text-center">{{ $subject->nama }}</td>
                            <td class="text-wrap text-center">{{ $subject->jumlah_sks }}</td>
                            <td class="d-flex justify-content-center">
                                <a class="btn btn-warning btn-sm rounded-pill mx-2" href="/pendaftaran-mata-kuliah">Daftar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
