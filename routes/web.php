<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/dosen', 'LecturerController@index');
Route::get('/dosen/create-new', 'LecturerController@create');
Route::post('/dosen/store', 'LecturerController@store');
Route::get('/dosen/{lecturers:slug}/edit', 'LecturerController@edit');
Route::patch('/dosen/{lecturers:slug}/edit', 'LecturerController@update');

Route::get('/mahasiswa', 'StudentController@index');
Route::get('/mahasiswa/create-new', 'StudentController@create');
Route::post('/mahasiswa/store', 'StudentController@store');
Route::get('/mahasiswa/{students:slug}/edit', 'StudentController@edit');
Route::patch('/mahasiswa/{students:slug}/edit', 'StudentController@update');

Route::get('/mata-kuliah', 'SubjectController@index');


Route::get('/pendaftaran-mata-kuliah', 'RegisterController@create');
